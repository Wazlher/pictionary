﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FBInteract : MonoBehaviour {

	public Texture pic;
	public Dictionary<string, string> infos;
	public string randomWord;
	public bool isConnected;

	private List<string> messageList;

	delegate void LoadPictureCallback (Texture texture);

	void Awake()
	{
		pic = null;
		infos = null;
		randomWord = null;
		enabled = false;
		isConnected = false;
		messageList = new List<string> ();
		FB.Init(SetInit, OnHideUnity); 
	}

	/* Login/logout & get basic informations */

	public void ConnectToFB()
	{
		FB.Login("email,publish_actions", LoginCallback);  
		
		pic = null;
		infos = null;
		randomWord = null;
	}

	public void DisconnectedToFB()
	{
		FB.Logout ();
		isConnected = false;
	}

	private void SetInit()                                                                       
	{                                                                                                                                                          
		if (FB.IsLoggedIn)                                                                       
		{                                                                             
			Debug.Log("Already logged in");                                                    
			OnLoggedIn();                                                                        
		}                                                                                        
	}                                                                                            
	
	private void OnHideUnity(bool isGameShown)                                                   
	{                                                                                                                                                        
		if (!isGameShown)                                                                                                                 
			Time.timeScale = 0;                                                                                                                                                      
		else                                                                                                                 
			Time.timeScale = 1;                                                                                                                                                        
	}

	void LoginCallback(FBResult result)                                                        
	{                                                                                          
		if (FB.IsLoggedIn)                                                                     
		{                                                                                      
			OnLoggedIn();
			isConnected = true;
		}                                                                                      
	}                                                                                          
	
	void OnLoggedIn()                                                                          
	{                                                                                          
		FB.API("/me?fields=name,first_name,birthday,email,link,name_format,locale", Facebook.HttpMethod.GET, APICallback);  
		LoadPictureAPI(Util.GetPictureURL("me", 100, 100),MyPictureCallback);   
	}

	void APICallback(FBResult result)                                                                                              
	{                                                                                                                                                                                                                           
		if (result.Error != null)                                                                                                  
		{                                                                                                                          
			Util.LogError(result.Error);                                                                                              
			return;                                                                                                                
		}                                                                                                                          
		
		infos = Util.DeserializeJSONProfile(result.Text);
	}                                                                                                                              

	/* Image loading */

	void MyPictureCallback(Texture texture)                                                                                        
	{                                                                                                                                                                                                                     	
		if (texture == null)                                                                                                  
		{                                                                                                                          
			Util.LogError("Can't load public profile photo");             
			return;                                                                                                                
		}                                                                                                                          
		pic = texture;
	}


	void LoadPictureAPI (string url, LoadPictureCallback callback)
	{
		FB.API(url,Facebook.HttpMethod.GET,result =>
		       {
			if (result.Error != null)
			{
				Util.LogError(result.Error);
				return;
			}
			
			var imageUrl = Util.DeserializePictureURLString(result.Text);
			
			StartCoroutine(LoadPictureEnumerator(imageUrl,callback));
		});
	}

	IEnumerator LoadPictureEnumerator(string url, LoadPictureCallback callback)    
	{
		WWW www = new WWW(url);
		yield return www;
		callback(www.texture);
	}

	void LoadPictureURL (string url, LoadPictureCallback callback)
	{
		StartCoroutine(LoadPictureEnumerator(url,callback));	
	}

	/* Publish a status (word or picture) on Facebook */

	public void PostStatusFBCallback(FBResult result)
	{
		Util.Log("PostStatusFBCallBack");                                                                                                
		if (result.Error != null)                                                                                                  
		{                                                                                                                          
			Util.LogError("FB.Feed error : " + result.Error);                                                                                            
			return;                                                                                                                
		}                                                                                                                          
	}

	public void PostStatusFB()
	{
			FB.Feed(
			"",
			"",
			"Pictionary",
			"#Pictionary",
			"",
			"http://www.vector-logo.net/logo_preview/eps/p/Pictionary.png",
			"",
			"http://apps.facebook.com/" + FB.AppId + "/?challenge_brag=" + (FB.IsLoggedIn ? FB.UserId : "guest"),
			"",
			"",
			null,
			null
			);
	}

	void GetSourceImageFB(FBResult result)
	{
		Util.Log("GetSourceImageFB");                                                                                                
		if (result.Error != null)                                                                                                  
		{                                                                                                                          
			Util.LogError(result.Error);                                                                                           
			return;                                                                                                                
		}
		Debug.Log (result.Text);
		string source = Util.DeserializeImageSource (result.Text);
		Debug.Log ("Source = " + source);
		FB.Feed(
			"",
			"",
			"",
			"Can you guess the word that I drew ?",
			"",
			source,
			"",
			"http://apps.facebook.com/" + FB.AppId + "/?challenge_brag=" + (FB.IsLoggedIn ? FB.UserId : "guest"),
			"",
			"",
			null,
			null
			);
	}

	void PostImageFBCallback(FBResult result)
	{
		Util.Log("PostImageFBCallback");                                                                                                
		if (result.Error != null)                                                                                                  
		{                                                                                                                          
			Util.LogError(result.Error);                                                                                           
			return;                                                                                                                
		}
		Debug.Log (result.Text);
		string id = Util.DeserializeImageId (result.Text);
		FB.API (id, Facebook.HttpMethod.GET, GetSourceImageFB);
	}

	public void PostImageOnAlbum(byte[] image)
	{
		var wwwForm = new WWWForm();
		wwwForm.AddBinaryData("image", image, "pictionary.png");
		
		FB.API("me/photos", Facebook.HttpMethod.POST, PostImageFBCallback, wwwForm);
	}

	/* Get Facebook's statuses and pick a random word */

	List<string> FillEmptyStatusesList()
	{
		List<string> words = new List<string> ();
		words.Add ("Lapin");
		words.Add ("Carotte");
		words.Add ("Soleil");
		words.Add ("Arbre");
		words.Add ("Lettre");
		words.Add ("Bouteille");
		words.Add ("Telephone");
		words.Add ("Sac");
		words.Add ("Echarpe");
		words.Add ("Lunette");
		words.Add ("Chaise");
		words.Add ("Voiture");
		return words;
	}

	public void ParseUserFeed(FBResult result)
	{
		if (result.Error != null)                                                                                                  
		{                                                                                                                          
			Util.LogError(result.Error);                                                                                                                                                                                       
			return;                                                                                                                
		} 

		List<string> messages = Util.DeserializeJSONFeed(result.Text);
		if (messages != null)
			messageList.AddRange(messages);
		if (messageList.Count == 0)
			messageList = FillEmptyStatusesList ();
		foreach (var lol in messageList)
						Debug.Log (lol);
		System.Random rnd = new System.Random();
		int idx = rnd.Next(messageList.Count);
		string[] words = messageList[idx].Split(' ');
		int nbTry = 0;
		do 
		{
			idx = rnd.Next (words.Length);
			nbTry++;
		}
		while (words[idx].CompareTo("#Pictionary") == 0 && nbTry < 50) ;
		string rndWord = words[idx];
		if (rndWord.CompareTo ("#Pictionary") == 0)
			rndWord = "Pictionary";
		randomWord = rndWord;
	}

	void ParseUserMessages(FBResult result)
	{                                                                                             
		if (result.Error != null)                                                                                                  
		{                                                                                                                          
			Util.LogError(result.Error);                                                                                                                                                                                    
			return;                                                                                                                
		}                                                                                                                      
		
		List<string> messages = Util.DeserializeJSONMessages(result.Text);
		if (messages != null)
			messageList.AddRange(messages);
		FB.API ("/me/feed", Facebook.HttpMethod.GET, ParseUserFeed);
	}

	public void PickRandomWord()
	{
		FB.API ("/me/statuses", Facebook.HttpMethod.GET, ParseUserMessages);
	}
}
