﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameGUI : MonoBehaviour {
	
	public Texture leftBackgroundMain;
	public Texture rightBackgroundMain;
	public Texture backgroundButtonPost;
	public Texture backgroundButtonPlay;
	public Texture loginTex;
	public Texture logoutTex;
	public Texture publishTex;
	public Texture homeTex;

	public Texture rubberTex;
	public Texture eraseTex;
	public Texture redTex;
	public Texture pinkTex;
	public Texture whiteTex;
	public Texture orangeTex;
	public Texture green1Tex;
	public Texture green2Tex;
	public Texture skyblueTex;
	public Texture blueTex;
	public Texture yellowTex;
	public Texture blackTex;
	public Texture purpleTex;
	public Texture brownTex;

	public FBInteract dataScript;

	public GUIStyle nameLabelStyle;
	public GUIStyle userinfoLabelStyle;
	public GUIStyle wordGuess;

	private bool allowClick;
	private float timeTreshold;
	private int scene; // 0 = homepage, 1 = drawpage;
	private Texture2D paintSurface;
	private Rect paintSurfacePos;
	private int brushSize;

	private Color currentColor;

	void Start () {
		allowClick = true;
		timeTreshold = 0;
		scene = 0;
		paintSurfacePos = new Rect (325, 60, 450, 250);
		currentColor = Color.black;
		brushSize = 10;
	}

	void Update ()
	{
		if (scene == 1 && Input.GetKey(KeyCode.Mouse0))
		{
			int mouseX = (int)Input.mousePosition.x;
			int mouseY = (int)Input.mousePosition.y;
			if (mouseX < paintSurfacePos.x || mouseX > paintSurfacePos.x + paintSurfacePos.width ||
			    mouseY < (Screen.height - (paintSurfacePos.y + paintSurfacePos.height)) || mouseY > (Screen.height - paintSurfacePos.y - 2))
				return;

			ApplyPixels();
		}
	}

	void OnGUI()
	{
		int width = Screen.width;
		int height = Screen.height;

		if (allowClick == false && timeTreshold < Time.time)
			allowClick = true;

		/* Homepage */
		if (scene == 0)
		{
			GUI.DrawTexture(new Rect(0, 0, width / 2, height), leftBackgroundMain);
			GUI.DrawTexture (new Rect (width / 2, 0, width / 2, height), rightBackgroundMain);
			
			if (dataScript == null)
				return;
			
			if (dataScript.isConnected)
			{
				Texture picture = dataScript.pic;
				if (picture != null)
						GUI.DrawTexture (new Rect (width * 0.6f, height * 0.25f, picture.width, picture.height), picture);
				Dictionary<string, string> dic = dataScript.infos;
				if (dic != null)
				{
					Rect pos = new Rect (width * 0.725f, height * 0.28f, 100, 22);
					if (dic.ContainsKey ("name"))
					{
						if (allowClick && dic.ContainsKey ("link") && GUI.Button (pos, dic ["name"], nameLabelStyle))
						{
							Application.OpenURL (dic ["link"]);
							allowClick = false;
							timeTreshold = Time.time + 0.15f;
						}
						pos.y += 30;
						}
						if (dic.ContainsKey ("birthday")) {
								GUI.Label (pos, dic ["birthday"], userinfoLabelStyle);
								pos.y += 27;
						}
						if (dic.ContainsKey ("email"))
								GUI.Label (pos, dic ["email"], userinfoLabelStyle);
				}
				if (GUI.Button (new Rect (width * 0.65f, height * 0.70f, 200, 50), logoutTex, "label"))
					dataScript.DisconnectedToFB();

				if (GUI.Button (new Rect(width * 0.17f, height * 0.35f, 150, 50), backgroundButtonPost, "label"))
					dataScript.PostStatusFB();
				if (GUI.Button (new Rect(width * 0.17f, height * 0.57f, 150, 50), backgroundButtonPlay,"label"))
				{
					dataScript.PickRandomWord();
					paintSurface = new Texture2D(450, 250);
					scene = 1;
				}
			}
			else
			{
				if (GUI.Button (new Rect (width * 0.65f, height * 0.40f, 200, 50), loginTex, "label"))
					dataScript.ConnectToFB();
			}
		}

		/* Drawpage */
		else if (scene == 1)
		{
			GUI.DrawTexture(new Rect(0, 0, width, height / 10), leftBackgroundMain);
			GUI.DrawTexture (new Rect (0, height / 10, width, height), rightBackgroundMain);
			
			if (dataScript == null)
				return;

			string msg = "Le mot a faire deviner est : " + dataScript.randomWord;
			GUI.Label(new Rect(width / 2 - 100, 6, 200, 20), msg, wordGuess);

			GUI.DrawTexture (paintSurfacePos, paintSurface);

			if (GUI.Button(new Rect(paintSurfacePos.x - 120, paintSurfacePos.y, 30, 30), rubberTex, "Label"))
				ChangeCurrentColor(214.0f, 212.0f, 207.0f);
			if (GUI.Button(new Rect(paintSurfacePos.x - 70, paintSurfacePos.y, 30, 30), eraseTex, "Label"))
				paintSurface = new Texture2D((int)paintSurfacePos.width, (int)paintSurfacePos.height);

			if (GUI.Button(new Rect(paintSurfacePos.x - 120, paintSurfacePos.y + 30.0f, 30, 30), redTex, "Label"))
				ChangeCurrentColor(255.0f, 0.0f, 0.0f);
			if (GUI.Button(new Rect(paintSurfacePos.x - 70, paintSurfacePos.y + 30.0f, 30, 30), orangeTex, "Label"))
				ChangeCurrentColor(255.0f, 138.0f, 0.0f);
			if (GUI.Button(new Rect(paintSurfacePos.x - 120, paintSurfacePos.y + 62.0f, 30, 30), yellowTex, "Label"))
				ChangeCurrentColor(246.0f, 255.0f, 0.0f);
			if (GUI.Button(new Rect(paintSurfacePos.x - 70, paintSurfacePos.y + 62.0f, 30, 30), pinkTex, "Label"))
				ChangeCurrentColor(252.0f, 0.0f, 255.0f);
			if (GUI.Button(new Rect(paintSurfacePos.x - 120, paintSurfacePos.y + 94.0f, 30, 30), green1Tex, "Label"))
				ChangeCurrentColor(2.0f, 255.0f, 18.0f);
			if (GUI.Button(new Rect(paintSurfacePos.x - 70, paintSurfacePos.y + 94.0f, 30, 30), skyblueTex, "Label"))
				ChangeCurrentColor(0.0f, 222.0f, 225.0f);
			if (GUI.Button(new Rect(paintSurfacePos.x - 120, paintSurfacePos.y + 126.0f, 30, 30), green2Tex, "Label"))
				ChangeCurrentColor(6.0f, 65.0f, 10.0f);
			if (GUI.Button(new Rect(paintSurfacePos.x - 70, paintSurfacePos.y + 126.0f, 30, 30), blueTex, "Label"))
				ChangeCurrentColor(0.0f, 12.0f, 255.0f);
			if (GUI.Button(new Rect(paintSurfacePos.x - 120, paintSurfacePos.y + 158.0f, 30, 30), whiteTex, "Label"))
				ChangeCurrentColor(255.0f, 255.0f, 255.0f);
			if (GUI.Button(new Rect(paintSurfacePos.x - 70, paintSurfacePos.y + 158.0f, 30, 30), blackTex, "Label"))
				ChangeCurrentColor(0.0f, 0.0f, 0.0f);
			if (GUI.Button(new Rect(paintSurfacePos.x - 70, paintSurfacePos.y + 190.0f, 30, 30), purpleTex, "Label"))
				ChangeCurrentColor(162.0f, 11.0f, 171.0f);
			if (GUI.Button(new Rect(paintSurfacePos.x - 120, paintSurfacePos.y + 190.0f, 30, 30), brownTex, "Label"))
				ChangeCurrentColor(87.0f, 36.0f, 42.0f);
			brushSize = (int)GUI.HorizontalSlider(new Rect(paintSurfacePos.x - 120, paintSurfacePos.y + 232.0f, 80, 20), (float)brushSize, 2.0f, 20.0f);
			

			if (GUI.Button(new Rect(width * 0.85f, height * 0.4f, 200, 25), homeTex, "label"))
			{
				paintSurface = new Texture2D((int)paintSurfacePos.width, (int)paintSurface.height);
				scene = 0;
			}

			if (GUI.Button(new Rect(width * 0.85f, height * 0.6f, 150, 25), publishTex, "label"))
			{
				CaptureScreenshot();
			}
		}
	}

	void ChangeCurrentColor(float r, float g, float b)
	{
		currentColor.r = r / 255.0f;
		currentColor.g = g / 255.0f;
		currentColor.b = b / 255.0f;
	}

	void ApplyPixels()
	{
		int len = brushSize * brushSize;
		Color[] col = new Color[len];
		for (int i = 0; i < len; ++i)
			col[i] = currentColor;
		
		float endY = Screen.height - (paintSurfacePos.y + paintSurfacePos.height);
		paintSurface.SetPixels((int)(Input.mousePosition.x - paintSurfacePos.x), (int)(Input.mousePosition.y - (Screen.height - (paintSurfacePos.y + paintSurfacePos.height))), brushSize, brushSize, col);
		paintSurface.Apply();
	}
	
	void CaptureScreenshot()
	{
		byte[] bytes = paintSurface.EncodeToPNG();
		dataScript.PostImageOnAlbum (bytes);
	}
}
